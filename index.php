<?php

require_once 'mysmarty.php';
$smarty = new MySmarty;

if ((isset($_POST['submit'])) and (!empty($_POST['text'])))
{

  require_once 'pigLatin.php';
  $pigLatin = new pigLatin;

  $pigLatin->pigIt($_POST['text']);

  $smarty->assign('translation', $pigLatin->showResult());

}

$smarty->display('index.tpl');

?>
