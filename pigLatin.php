<?php

  class pigLatin {

    private $result;
    private $message;

    //private function

    private function isVowel($letter)
    {

      if(preg_match("/[aeiou]/i", $letter))
      {

        return TRUE;

      }else{

        return FALSE;

      }

    }

    private function isSpecChar($letter)
    {

      if(preg_match("/[\-\_\(\)\'\.\,\?\!\:]/", $letter))
      {

        return TRUE;

      }else{

        return FALSE;

      }

    }

    private function separate($text)
    {

      $sep = explode(' ', $text);

      return $sep;

    }

    private function isNum($word)
    {

      if(preg_match("/[0-9]$/i", $word)){

        return TRUE;

      }else{

        return FALSE;

      }

    }

    private function mashUp($word)
    {

      $letters = str_split($word);

      $c = count($letters);

      $c--;

      if($c >= 2)
      {

        $goLast = NULL;

        if($this->isSpecChar($letters[0]))
        {

          $goFirst = $letters[0];

          $word = ltrim($word, $word[0]);

        }

        if($this->isSpecChar($letters[$c]))
        {

          $goToEnd = $letters[$c];

          $word = rtrim($word, $word[$c]);

        }

        for($i = 0; $i < $c; $i++)
        {

          if(!$this->isVowel($letters[$i]))
          {

            if(!$this->isSpecChar($letters[$i]))
            {

              if (is_null($goLast))
              {

                $goLast = array(strtolower($letters[$i]));

              }else{

                array_push($goLast, strtolower($letters[$i]));

              }

              $word = ltrim($word, $word[0]);

            }

          }else{

            $i = $c;

          } //isVowel

        } //for every letter until isVowel

        if(isset($goFirst))
        {

          $word = $goFirst . $word;

        }

        if(isset($goLast))
        {

          $word = $word . "-";

          foreach ($goLast as $key => $letter) {

            $word = $word . $letter;

          }

          $word = $word . "ay";

        }else{

          $rnd = rand(0, 2);

          $vowends = array("'yay", "'hay", "'way");

          $word = $word . $vowends[$rnd];

        }

        if(isset($goToEnd))
        {

          $word = $word . $goToEnd;

        }

      } //at least 2 letters

      return $word;

    } //mashUp function


    //public functions

    public function pigIt($text)
    {

      $words = explode(' ', $text);

      $result = "";

      foreach ($words as $key => $word) {

        if(!$this->isNum($word))
        {

          $words[$key] = $this->mashUp($word);

        }

      }

      $this->result = implode(" ", $words);
      
    }


    public function showResult()
    {

      if(!is_null($this->result))
      {

        return $this->result;

      }else{

        $this->message = "Nejsou žádné výsledky k zobrazení.";

      }

    }

    public function showMessage()
    {

      return $this->message;

    }

  } //end of class pigLatin

?>
